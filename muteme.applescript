#!/usr/bin/osascript

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title muteme
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🤖

# Documentation:
# @raycast.author cy


on getMicrophoneVolume()
	input volume of (get volume settings)
end getMicrophoneVolume

on getOutputVolume()
	output volume of (get volume settings)
end getOutputVolume

if getMicrophoneVolume() is greater than 0 then
	# remember outputVolume original value
	local outputVolume
	set outputVolume to getOutputVolume()
	
	# set input volume to 0, (mute)
	set volume input volume 0

	# set output volume back to original value (in MacOS Monterey this gets set to 0 when input Volume is being set to 0)
	set volume output volume outputVolume 
	
	
	say "muted" using "Samantha"
	display notification "Microphone Muted"
else
	set volume input volume 100
	say "unmuted" using "Samantha"
	display notification "Microphone Activated"
end if
