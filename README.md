# Mute and Unmute microphone through raycast.app

allows macOS to silence yourself through a global hotkey. 

## what is this?

this is a snippet of an applescript that allows muting/unmuting your microphone through a global hotkey with raycast

you can also run the script through automator and add a system hotkey, however i found raycast to react much faster when a hotkey is pressed

## why do i need this

this is useful while talking on remote meetings, especially if you have no focus on your Zoom, Teams, Meet or whatever tool, e.g. when presenting or pair-programming.
Since most videochat tools do not allow for a global mute hotkey, this script allows you to switch the microphone input level of your macOS system Sound app to zero or max (or whatever you change the value to yourself)

my coworkers do not appreciate my mechanical keyboard and me snoring in boring meetings, so i can just hit the mute hotkey-combination, no matter what app i am currently using in the foreground.

## how does it work?

the script explains itself. 
- if your current microphone input level is zero (you are muted), it will set it to a value of 100, granting maximum intake
- if your mic level differs from zero, it sets it to 0, therefor muting you.
- to make the script more verbose and prevent accidental toggling, it features a voice message and a notification. neither of those are needed for the actual script to work, you can delete either, change the voice, change the notification text or add more cosmetics around it.

### how do i add the hotkey?

- get raycast.app from raycast.io (which is basically Spotlight on steroids)
- create a script command folder in raycast and copy this mute script there (or link it)
- add hotkey combination of choice to the script command in raycast
- mute/unmute yourself


(the script does also work without raycast through automator. i have documented an automator howto with a prior version of this script in https://gitlab.com/cy4n/mic-toggle-mac, however i found raycast to work MUCH faster. Automator sometimes delays script execution by about 1-3 seconds (just my feelings, never measured). Aside of this you want to use raycast anyway over spotlight for all the other features so it's already there anyway.)

